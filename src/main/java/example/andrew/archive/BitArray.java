package example.andrew.archive;

public class BitArray {

    private String id;
    char arr[];
    int highVer = 599999999, iHighVer = highVer/16;
    private final static int size = 600000000;
    private final static int intLen = size/16+1;
    private final static char twoPower[] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024,
            2048, 4096, 8192, 16384, 32768};

    BitArray(String s){
        try {
            id = s;
            arr = new char[intLen];
        }catch (OutOfMemoryError e){
            e.printStackTrace();
        }
    }

    public boolean add(int pos){
        int i = pos >> 4;
        int j = pos % 16;
        int temp = arr[i];
        arr[i] |= twoPower[j];
        return temp != arr[i];
    }

    public void reset(int pos){
        int i = pos >> 4;
        int j = pos % 16;
        arr[i] &= ~twoPower[j];
    }

    public boolean get(int pos){
        try {
            int i = pos >> 4;
            int j = pos % 16;
            return (arr[i] | twoPower[j]) == arr[i];
        }catch (ArrayIndexOutOfBoundsException exc){return false;}
    }

    public boolean get(int i, int j){
        try {
            return (arr[i] | twoPower[j]) == arr[i];
        }catch (ArrayIndexOutOfBoundsException exc){return false;}
    }

    public void print(){
        for (int i=0; i<=iHighVer; i++){
            System.out.print(i+".  ");
            for (int f=0; f<16; f++){
                if (get(i, f)) System.out.print("1 ");
                else System.out.print("0 ");
            }
            System.out.println();
        }
    }
}
