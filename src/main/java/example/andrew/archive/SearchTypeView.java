package example.andrew.archive;

import javafx.fxml.FXMLLoader;

import java.io.IOException;

public class SearchTypeView extends FXMLLoader implements SearchTypeViewListener {

    SearchTypeView(){
        setLocation(getClass().getResource("../resources/layout/searchtype.fxml"));
        try {
            load();
        }catch (IOException exc){
            exc.printStackTrace();
        }
        ((SearchTypeController) getController()).setListener(this);
    }

    @Override
    public void onButIdClick(){

    }

    @Override
    public void onButFriendsClick(){

    }

    @Override
    public void onButFollowsClick(){

    }

    @Override
    public void onButStarredClick(){

    }
}
