package example.andrew.archive;

public interface SearchTypeViewListener {

    void onButIdClick();
    void onButFriendsClick();
    void onButFollowsClick();
    void onButStarredClick();
}
