package example.andrew.archive;

import javafx.fxml.FXML;

public class SearchTypeController {

    private SearchTypeViewListener listener;

    @FXML
    public void onButIdClick(){
        listener.onButIdClick();
    }

    @FXML
    public void onButFriendsClick(){
        listener.onButFriendsClick();
    }

    @FXML
    public void onButFollowsClick(){
        listener.onButFollowsClick();
    }

    @FXML
    public void onButStarredClick(){
        listener.onButStarredClick();
    }

    public void setListener(SearchTypeViewListener listener){
        this.listener = listener;
    }
}
