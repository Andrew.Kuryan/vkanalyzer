package example.andrew;

import org.mt.vk.user.LightUser;
import javafx.concurrent.WorkerStateEvent;
import javafx.scene.image.Image;
import example.andrew.loaders.AvaLoader;
import example.andrew.loaders.AvaQueueLoader;

import java.util.ArrayList;
import java.util.Collections;

public class Tree {

    private AvaQueueLoader avaQueueLoader;

    public int startID, finishID;
    public static final int maxDeep = 6;
    public ArrayList<Unit> u;
    public ArrayList<Edge> e;
    public ArrayList<ArrayList<Edge>> listOfLinks;

    private int maxLevel;
    private int maxCount;
    private int maxLevelCount;
    private int numOfUnits[];
    private int numOfLinks;

    public int getNumOfUnits(int lev){
        return numOfUnits[lev];
    }

    public int getNumOfLevels() {
        return maxLevel;
    }

    public int getMaxLevelCount(){
        return maxLevelCount;
    }

    public int getMaxCount(){
        return maxCount;
    }

    public int getNumOfLinks(){
        return numOfLinks;
    }

    public Unit getStartUser(){
        for (Unit unit : u){
            if (unit.ID == startID)
                return unit;
        }
        return null;
    }

    public Unit getFinishUser(){
        for (Unit unit : u){
            if (unit.ID == finishID)
                return unit;
        }
        return null;
    }


    public boolean isLoading(){
        return !avaQueueLoader.isDone();
    }

    public class Unit extends LightUser {

        public int level;
        public int numOfChildrens = 0;
        public Image photo;

        Unit(LightUser u){
            super(u.ID);
            level = 0;
            this.first_name = u.first_name;
            this.last_name = u.last_name;
            this.avaSmall = u.avaSmall;
            AvaLoader loader = new AvaLoader();
            loader.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, (workerStateEvent)->
                    photo = loader.getValue()
            );
            loader.addEventHandler(WorkerStateEvent.WORKER_STATE_FAILED, (workerStateEvent)->
                    loader.getException().printStackTrace()
            );
            avaQueueLoader.addToQueue(loader, avaSmall);
        }

        Unit(LightUser u, Unit parent){
            super(u.ID);
            level = parent.level+1;
            this.first_name = u.first_name;
            this.last_name = u.last_name;
            this.avaSmall = u.avaSmall;
            AvaLoader loader = new AvaLoader();
            loader.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, (workerStateEvent)->
                    photo = loader.getValue()
            );
            loader.addEventHandler(WorkerStateEvent.WORKER_STATE_FAILED, (workerStateEvent)->
                    loader.getException().printStackTrace()
            );
            avaQueueLoader.addToQueue(loader, avaSmall);
        }

        @Override
        public String toString(){
            return "[".concat(Integer.toString(ID)).concat("]");
        }
    }

    public class Edge {

        public Unit vert1, vert2;

        Edge(Unit v1, Unit v2) {
            vert1 = v1;
            vert2 = v2;
        }

        public boolean equals(Edge ed) {
            return vert1.ID == ed.vert1.ID && vert2.ID == ed.vert2.ID;
        }

        @Override
        public String toString(){
            return "[".concat(Integer.toString(vert1.ID)).concat(" <-> ").concat(Integer.toString(vert2.ID)).concat("]");
        }
    }

    public Tree(LightUser start, LightUser finish){
        avaQueueLoader = new AvaQueueLoader();
        avaQueueLoader.start();
        startID = start.ID;
        finishID = finish.ID;

        u = new ArrayList<>();
        u.add(new Unit(start));
        e = new ArrayList<>();
        listOfLinks = new ArrayList<>();
        System.out.println("Create tree");
    }

    public Edge findEdge(int startID, int finishID){
        for (Edge edge : e){
            if ((edge.vert1.ID == startID && edge.vert2.ID == finishID)
                || (edge.vert2.ID == startID && edge.vert1.ID == finishID))
                return edge;
        }
        return null;
    }

    public void addLink(Edge...edges){
        ArrayList<Edge> al = new ArrayList<>();
        Collections.addAll(al, edges);
        listOfLinks.add(al);
    }

    public Edge addEdge(LightUser uFrom, LightUser uTo){
        if (uFrom.ID == uTo.ID)
            return null;
        int posFrom = -1, posTo = -1;
        for (int i = 0; i < u.size(); i++) {
            if (posFrom == -1 && u.get(i).ID == uFrom.ID) {
                posFrom = i;
            }
            if (posTo == -1 && u.get(i).ID == uTo.ID) {
                posTo = i;
            }
        }
        if (posFrom == -1) {
            System.err.println("Несуществующий родительский юнит: " + uFrom.ID);
            return null;
        }
        if (posTo == -1) {
            posTo = u.size();
            u.add(new Unit(uTo, u.get(posFrom)));
        }
        //проверка на существование такого же ребра
        Edge ed = new Edge(u.get(posFrom), u.get(posTo));
        for (Edge edge : e) {
            if (ed.equals(edge))
                return null;
        }
        //System.out.println("Add Edge: from "+uFrom.ID+" to "+uTo.ID);
        //корректировка уровней
        if(u.get(posFrom).level + 1 != u.get(posTo).level){
            u.get(posTo).level = u.get(posFrom).level + 1;
        }
        u.get(posFrom).numOfChildrens++;
        e.add(ed);
        return ed;
    }



    public void calcLayout(){
        System.out.println("\nCalc Layout:");
        System.out.println("---------------------------");
        calcNumOfUnit();
        calcNumOfLevels();
        calcMaxLevelCount();
        calcMaxCount();

        for (ArrayList<Edge> link : listOfLinks){
            for (Edge e : link){
                System.out.print(e+" ");
            }
            System.out.println();
        }
    }

    //количество юнитов на каждом уровне
    private void calcNumOfUnit(){
        numOfUnits = new int[maxDeep];
        for (int i=0; i<maxDeep; i++){
            int count = 0;
            for (Unit unit : u){
                if (unit.level == i)
                    count++;
            }
            numOfUnits[i] = count;
        }

        for (int i=0; i<Tree.maxDeep; i++)
            System.out.println("level "+i+" : "+numOfUnits[i]);
    }

    //количество уровней
    private void calcNumOfLevels(){
        int count = 0;
        for (int i = 0; i < maxDeep; i++) {
            if (numOfUnits[i] != 0)
                count++;
            else
                break;
        }
        System.out.println("Num of levels: " + count);
        this.maxLevel = count;
    }

    //максимальное количество юнитов на одном уровне
    private void calcMaxLevelCount(){
        int max = 0;
        for (int i=0; i<maxDeep; i++){
            if (numOfUnits[i] > max)
                max = getNumOfUnits(i);
        }
        System.out.println("Max level count: "+max);
        this.maxLevelCount = max;
    }

    //максимальное количество дочерних узлов у одного юнита
    private void calcMaxCount(){
        int max = 0;
        for (Unit unit : u){
            if (unit.numOfChildrens > max)
                max = unit.numOfChildrens;
        }
        System.out.println("Max count: "+max);
        System.out.println("---------------------------\n");
        this.maxCount = max;
    }

    public void setNumOfLinks(int numOfLinks){
        this.numOfLinks = numOfLinks;
    }




    @Override
    public String toString(){
        String res = "";
        res = res.concat("Вершины: ");
        for (Unit unit:u){
            res = res.concat(unit.toString()).concat(", ");
        }
        res = res.concat("\nРебра: ");
        for (Edge edge:e) {
            res = res.concat(edge.toString()).concat(", ");
        }
        return res;
    }
}
