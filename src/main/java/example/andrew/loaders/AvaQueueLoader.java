package example.andrew.loaders;

import javafx.concurrent.Task;

import java.util.LinkedList;
import java.util.Queue;

public class AvaQueueLoader extends Task<Void> {

    private Queue<AvaLoader> queue;
    private Queue<String> urlQueue;
    private boolean flag_run;

    public AvaQueueLoader(){
        queue = new LinkedList<>();
        urlQueue = new LinkedList<>();
        flag_run = false;
    }

    public void start(){
        flag_run = true;
        new Thread(this).start();
    }

    public void addToQueue(AvaLoader loader, String url){
        queue.add(loader);
        urlQueue.add(url);
        if (flag_run && isDone()){
            call();
        }
    }

    @Override
    protected Void call(){
        AvaLoader loader = queue.poll();
        while (loader != null){
            loader.start(urlQueue.poll(), null);
            while (!loader.isDone()){
                //ожидание загрузки
            }
            loader = queue.poll();
        }
        return null;
    }
}
