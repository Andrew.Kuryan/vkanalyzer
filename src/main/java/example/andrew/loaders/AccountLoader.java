package example.andrew.loaders;

import org.mt.vk.RequestException;
import org.mt.vk.authorization.Account;
import org.mt.vk.authorization.Authorization;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressIndicator;

public class AccountLoader extends Task<Account> {

    private Account oldVal;
    private AvaLoader avaLoader;
    private ProgressIndicator indicator;

    public AccountLoader(Account oldVal, AvaLoader avaLoader, ProgressIndicator indicator){
        this.oldVal = oldVal;
        this.avaLoader = avaLoader;
        this.indicator = indicator;
    }

    public void start(){
        if (indicator != null) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    indicator.setVisible(true);
                }
            });
        }
        new Thread(this).start();
    }

    @Override
    protected Account call() throws RequestException{
        try {
            Authorization.loadAccount(oldVal.ID, true);
        }catch (RequestException exc){
            exc.printStackTrace();
            setException(exc);
            throw exc;
        }
        return Authorization.curAcc;
    }

    @Override
    protected void failed(){
        if (indicator != null) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    indicator.setVisible(false);
                }
            });
        }
        avaLoader.cancel();
    }

    @Override
    protected void succeeded(){
        avaLoader.start(oldVal.avaSmall, indicator);
    }
}
