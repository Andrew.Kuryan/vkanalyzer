package example.andrew.loaders;

import org.mt.vk.RequestException;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class AvaLoader extends Task<Image> {

    private String url;
    private ProgressIndicator indicator;

    public AvaLoader(){}

    public void start(String url, ProgressIndicator indicator){
        this.url = url;
        this.indicator = indicator;
        new Thread(this).start();
    }

    @Override
    protected Image call() throws RequestException {
        Image image = null;
        try {
            URL address = new URL(url);
            InputStream is = address.openStream();
            image = new Image(is);
        }catch (IOException exc){
            RequestException re = new RequestException(500);
            re.printStackTrace();
            setException(re);
            throw re;
        }
        return image;
    }

    @Override
    protected void succeeded(){
        if (indicator != null) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    indicator.setVisible(false);
                }
            });
        }
    }
}
