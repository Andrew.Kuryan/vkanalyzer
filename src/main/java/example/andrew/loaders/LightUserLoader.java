package example.andrew.loaders;

import org.mt.vk.RequestException;
import org.mt.vk.user.LightUser;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressIndicator;

public class LightUserLoader extends Task<LightUser> {

    private ProgressIndicator indicator;
    private String screen_name;
    private AvaLoader avaLoader;
    private LightUser user;

    public LightUserLoader(String screen_name, AvaLoader avaLoader, ProgressIndicator indicator){
        this.indicator = indicator;
        this.screen_name = screen_name;
        this.avaLoader = avaLoader;
    }

    public void start(){
        if (indicator != null) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    indicator.setVisible(true);
                }
            });
        }
        new Thread(this).start();
    }

    @Override
    protected LightUser call() throws RequestException{
        user = new LightUser(screen_name);
        try {
            user.loadLightUser();
        }catch (RequestException exc){
            exc.printStackTrace();
            setException(exc);
            throw exc;
        }
        return user;
    }

    @Override
    protected void failed(){
        if (indicator != null) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    indicator.setVisible(false);
                }
            });
        }
        avaLoader.cancel();
    }

    @Override
    protected void succeeded(){
        avaLoader.start(user.avaSmall, indicator);
    }
}
