package example.andrew.loaders;

import org.mt.vk.RequestException;
import org.mt.vk.user.User;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressIndicator;

public class UserLoader extends Task<User> {

    private ProgressIndicator indicator;
    private String screen_name;
    private AvaLoader avaLoader;
    private User user;

    public UserLoader(String screen_name, AvaLoader avaLoader, ProgressIndicator indicator){
        this.indicator = indicator;
        this.screen_name = screen_name;
        this.avaLoader = avaLoader;
    }

    public void start(){
        if (indicator != null) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    indicator.setVisible(true);
                }
            });
        }
        new Thread(this).start();
    }

    @Override
    protected User call() throws RequestException{
        user = new User(screen_name);
        try {
            user.loadUser();
        }catch (RequestException exc){
            exc.printStackTrace();
            setException(exc);
            throw exc;
        }
        return user;
    }

    @Override
    protected void failed(){
        if (indicator != null) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    indicator.setVisible(false);
                }
            });
        }
        avaLoader.cancel();
    }

    @Override
    protected void succeeded(){
        avaLoader.start(user.avaBig, indicator);
    }
}
