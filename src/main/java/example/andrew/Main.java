package example.andrew;

import org.mt.vk.authorization.Authorization;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import example.andrew.controllers.MainViewController;
import example.andrew.interfaces.MainListener;
import org.mt.vk.user.User;
import example.andrew.views.AccountsView;
import example.andrew.views.MainView;

public class Main extends Application implements MainListener {

    private final static double xSize = 1000, ySize = 650;

    private Stage primaryStage;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        primaryStage.setTitle("VKAnalyzer");
        Authorization.setCacheDirectory("src/main/resources/cache/");
        Authorization.checkForAccounts();
        setAccountsScene();
        primaryStage.show();
    }

    public void setAccountsScene(){
        AccountsView accountView = new AccountsView();
        accountView.setListener(this);
        Scene accountScene = new Scene(accountView.getRoot(), xSize, ySize);
        primaryStage.setScene(accountScene);
    }

    public void setMainScene(){
        MainView mainView = new MainView();
        mainView.setListener(this);
        ((MainViewController) mainView.getController()).setAccount(Authorization.curAcc);
        Scene mainScene = new Scene(mainView.getRoot(), xSize, ySize);
        primaryStage.setScene(mainScene);
    }

    @Override
    public void onAccChoose(int id){
        Authorization.setCurrent(id);
        setMainScene();
        System.gc();
    }

    @Override
    public void changeUser(){
        setAccountsScene();
        System.gc();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
