package example.andrew;

import org.mt.vk.RequestException;
import org.mt.vk.user.LightUser;
import org.mt.vk.userLists.Friends;
import javafx.concurrent.Task;

import java.util.ArrayList;

public class Searcher extends Task<Tree>{

    private LightUser start, finish;
    private int deep, count;
    private long startTime;

    public Searcher(LightUser start, LightUser finish, int deep){
        this.start = start;
        this.finish = finish;
        this.deep = deep;
    }

    public LightUser getStartUser(){
        return start;
    }

    public LightUser getFinishUser(){
        return finish;
    }

    public int getDeep(){
        return deep;
    }

    public int getCount(){
        return count;
    }

    public void start(long startTime){
        this.startTime = startTime;
        new Thread(this).start();
    }

    @Override
    protected Tree call() throws RequestException{
        Tree tree = new Tree(start, finish);

        count = 0;
        updateMessage(Integer.toString(count));
        //проверка для глубины 1
        ArrayList<LightUser> list1, list2;
        try{
            list1 = loadFriendsList(start);
        }catch (RequestException exc){
            exc.printStackTrace();
            throw exc;
        }
        //в list1 список друзей первого пользователя
        for (LightUser user : list1){
            if (user.ID == finish.ID){
                if (tree.addEdge(start, finish) != null){
                    count++;
                    updateMessage(Integer.toString(count));
                    tree.addLink(tree.findEdge(start.ID, finish.ID));
                }
            }
        }
        if (deep == 1) {
            while (tree.isLoading()){
                //ожидание завершения загрузки фотографий
            }
            return tree;
        }

        //проверка для глубины 2
        try{
            list2 = loadFriendsList(finish);
        }catch (RequestException exc){
            exc.printStackTrace();
            throw exc;
        }
        //в list2 список друзей второго пользователя
        for (LightUser user : list2){
            if (isContain(list1, user)){
                if (tree.addEdge(start, user) != null
                        | tree.addEdge(user, finish) != null) {
                    count++;
                    updateMessage(Integer.toString(count));
                    tree.addLink(tree.findEdge(start.ID, user.ID),
                            tree.findEdge(user.ID, finish.ID));
                }
            }
        }
        if (deep == 2) {
            while (tree.isLoading()) {
                //ожидание завершения загрузки фотографий
            }
            return tree;
        }

        //проверка для глубины 3
        for (LightUser lu : list1) {
            if (lu.ID == finish.ID)
                continue;
            ArrayList<LightUser> list11;
            try{
                list11 = loadFriendsList(lu);
            }catch (RequestException exc){
                exc.printStackTrace();
                throw exc;
            }
            for (LightUser user : list11){
                if (user.ID == start.ID || user.ID == finish.ID)
                    continue;
                if (isContain(list2, user)){
                    if (tree.addEdge(start, lu) != null
                            | tree.addEdge(lu, user) != null
                            | tree.addEdge(user, finish) != null){
                        count++;
                        updateMessage(Integer.toString(count));
                        tree.addLink(tree.findEdge(start.ID, lu.ID),
                                        tree.findEdge(lu.ID, user.ID),
                                        tree.findEdge(user.ID, finish.ID));
                    }
                }
            }
        }
        if (deep == 3) {
            while (tree.isLoading()) {
                //ожидание завершения загрузки фотографий
            }
            return tree;
        }
        return null;
    }

    private boolean isContain(ArrayList<LightUser> al, LightUser user){
        for (LightUser lu : al){
            if (lu.ID == user.ID)
                return true;
        }
        return false;
    }

    private ArrayList<LightUser> loadFriendsList(LightUser user) throws RequestException{
        Friends friends = new Friends(user);
        ArrayList<LightUser> list11;
        try{
            list11 = friends.loadUsersList();
            System.out.print(user.ID +" friends completed: ");
            System.out.println(list11);
            //for (LightUser user : list11)
            //    System.out.print(user.ID+", ");
            //System.out.println();
        }catch (RequestException exc){
            int err = exc.getErrorCode();
            if (err == 500 || err == 17 || err == 14 || err == 5) {
                setException(exc);
                throw exc;
            }
            else
                list11 = new ArrayList<>();
        }
        return list11;
    }
}