package example.andrew.views;

import javafx.fxml.FXMLLoader;
import example.andrew.controllers.AuthViewController;
import example.andrew.interfaces.AccountsViewListener;
import example.andrew.interfaces.AuthViewListener;

import java.io.IOException;

public class AuthView extends FXMLLoader implements AuthViewListener {

    private AccountsViewListener listener;

    public AuthView(){
        setLocation(getClass().getResource("/layout/authview.fxml"));
        try {
            load();
        }catch (IOException exc){
            exc.printStackTrace();
        }
        ((AuthViewController) getController()).setListener(this);
        ((AuthViewController) getController()).loadAuth();
    }

    public void onButReset(){
        listener.onAuthFailed();
    }

    public void onAuthComplete(String ans){
        listener.onAuthComplete(ans);
    }

    public void onAuthFailed(){
        listener.onAuthFailed();
    }

    public void setListener(AccountsViewListener listener){
        this.listener = listener;
    }
}
