package example.andrew.views;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import example.andrew.controllers.ResultViewController;

import java.io.IOException;

public class ResultView extends FXMLLoader{

    public ResultView(){
        setLocation(getClass().getResource("/layout/resultview.fxml"));
        try {
            load();
        }catch (IOException exc){
            exc.printStackTrace();
        }
    }

    public void setContent(Node content){
        ((ResultViewController) getController()).setContent(content);
    }

    public void bindNumProperty(ReadOnlyStringProperty property){
        ((ResultViewController) getController()).bindNumProperty(property);
    }

    public void setNumOfLinks(int num){
        ((ResultViewController) getController()).setNumOfLinks(num);
    }

    public void setProcessName(String name){
        ((ResultViewController) getController()).setProcessName(name);
    }
}
