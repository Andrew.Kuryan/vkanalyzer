package example.andrew.views;

import org.mt.vk.user.User;
import javafx.fxml.FXMLLoader;
import example.andrew.controllers.UserViewController;

import java.io.IOException;

public class UserView extends FXMLLoader {

    public UserView(){
        setLocation(getClass().getResource("/layout/userview.fxml"));
        try {
            load();
        }catch (IOException exc){
            exc.printStackTrace();
        }
    }

    public User getUser(){
        return ((UserViewController) getController()).user;
    }

    public void loadUser(String screen_name){
        ((UserViewController) getController()).loadUser(screen_name);
    }
}
