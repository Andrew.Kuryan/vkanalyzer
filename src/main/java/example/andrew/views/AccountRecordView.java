package example.andrew.views;

import org.mt.vk.authorization.Account;
import javafx.fxml.FXMLLoader;
import example.andrew.controllers.AccountRecordViewController;

import java.io.IOException;

public class AccountRecordView extends FXMLLoader {

    public AccountRecordView(Account account){
        setLocation(getClass().getResource("/layout/accountrecordview.fxml"));
        try {
            load();
        }catch (IOException exc){
            exc.printStackTrace();
        }
        AccountRecordViewController arc = getController();
        arc.setId(account.ID);
        if (!account.first_name.isEmpty() && !account.last_name.isEmpty())
            arc.setName(account.first_name, account.last_name);
    }
}
