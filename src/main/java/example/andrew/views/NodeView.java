package example.andrew.views;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.layout.Pane;
import example.andrew.Tree;
import example.andrew.controllers.NodeViewController;
import example.andrew.interfaces.MainViewListener;

import java.io.IOException;

public class NodeView extends FXMLLoader {

    private MainViewListener listener;

    public NodeView(Tree.Unit unit){
        setLocation(getClass().getResource("/layout/nodeview.fxml"));
        try {
            load();
        }catch (IOException exc){
            exc.printStackTrace();
        }
        ((NodeViewController) getController()).setAva(unit.photo);
        ((NodeViewController) getController()).setNum(unit.numOfChildrens);

        Tooltip tp = new Tooltip(unit.first_name+" "+unit.last_name);
        Tooltip.install(getNode(), tp);

        final ContextMenu contextMenu = new ContextMenu();
        MenuItem copy = new MenuItem("Скопировать ссылку");
        copy.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Clipboard clipboard = Clipboard.getSystemClipboard();
                ClipboardContent content = new ClipboardContent();
                content.putString("https://vk.com/id".concat(Integer.toString(unit.ID)));
                clipboard.setContent(content);
            }
        });
        SeparatorMenuItem smi1 = new SeparatorMenuItem();
        MenuItem useAsFirst = new MenuItem("Использовать как 1-го пользователя");
        useAsFirst.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if (listener == null) {
                    System.out.println("Listener is null");
                }
                if (unit == null) {
                    System.out.println("Unit is null");
                }
                listener.loadFirstUser(unit.ID);
            }
        });
        MenuItem useAsSecond = new MenuItem("Использовать как 2-го пользователя");
        useAsSecond.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                listener.loadSecondUser(unit.ID);
            }
        });
        contextMenu.getItems().addAll(copy, smi1, useAsFirst, useAsSecond);

        getNode().setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
            @Override
            public void handle(ContextMenuEvent contextMenuEvent) {
                contextMenu.show(getNode(), contextMenuEvent.getScreenX(), contextMenuEvent.getScreenY());
            }
        });
    }

    public Node getNode(){
        return (Pane) getRoot();
    }

    public void setListener(MainViewListener listener){
        this.listener = listener;
    }
}
