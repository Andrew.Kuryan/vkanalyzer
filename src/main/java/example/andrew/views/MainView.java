package example.andrew.views;

import org.mt.vk.user.User;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.FlowPane;
import example.andrew.controllers.MainViewController;
import example.andrew.interfaces.MainListener;
import example.andrew.interfaces.MainViewListener;

import java.io.IOException;

public class MainView extends FXMLLoader implements MainViewListener {

    private MainListener listener;

    private UserView userView1;
    private UserView userView2;

    public MainView(){
        setLocation(getClass().getResource("/layout/mainview.fxml"));
        try {
            load();
        }catch (IOException exc){
            exc.printStackTrace();
        }
        ((MainViewController) getController()).setListener(this);
        userView1 = new UserView();
        ((FlowPane) getNamespace().get("leftPane")).getChildren().add(userView1.getRoot());
        userView2 = new UserView();
        ((FlowPane) getNamespace().get("rightPane")).getChildren().add(userView2.getRoot());
    }

    @Override
    public User getFirstUser(){
        return userView1.getUser();
    }

    @Override
    public User getSecondUser(){
        return userView2.getUser();
    }

    @Override
    public void loadFirstUser(int id){
        userView1.loadUser(Integer.toString(id));
    }

    @Override
    public void loadSecondUser(int id){
        userView2.loadUser(Integer.toString(id));
    }

    @Override
    public void changeAccount(){
        listener.changeUser();
    }

    public void setListener(MainListener listener){
        this.listener = listener;
    }
}
