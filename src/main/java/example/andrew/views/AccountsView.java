package example.andrew.views;

import org.mt.vk.authorization.Account;
import org.mt.vk.authorization.Authorization;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.StackPane;
import example.andrew.controllers.AccountRecordViewController;
import example.andrew.controllers.AccountsViewController;
import example.andrew.interfaces.AccountsViewListener;
import example.andrew.interfaces.MainListener;
import example.andrew.loaders.AccountLoader;
import example.andrew.loaders.AvaLoader;

import java.io.IOException;

public class AccountsView extends FXMLLoader implements AccountsViewListener {

    private MainListener listener;

    public AccountsView(){
        setLocation(getClass().getResource("/layout/accountsview.fxml"));
        try {
            load();
        } catch (IOException exc){
            exc.printStackTrace();
        }
        ((AccountsViewController) getController()).setListener(this);
        for (Account account : Authorization.getAll()){
            Authorization.setCurrent(account.ID);
            AccountRecordView arv = new AccountRecordView(account);
            ((AccountsViewController) getController()).addAccount(account.ID, arv.getRoot());
            ((AccountRecordViewController) arv.getController()).setListener(this);

            AvaLoader avaLoader = new AvaLoader();
            avaLoader.start(account.avaSmall, null);
            avaLoader.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, new EventHandler<WorkerStateEvent>() {
                @Override
                public void handle(WorkerStateEvent workerStateEvent) {
                    ((AccountRecordViewController) arv.getController()).setAva(avaLoader.getValue());
                }
            });
            avaLoader.addEventHandler(WorkerStateEvent.WORKER_STATE_FAILED, new EventHandler<WorkerStateEvent>() {
                @Override
                public void handle(WorkerStateEvent workerStateEvent) {
                    ((AccountsViewController) getController()).showError(avaLoader.getException().toString());
                }
            });
        }
    }

    public void setListener(MainListener listener){
        this.listener = listener;
    }

    @Override
    public void onButAddClick(){
        AuthView authView = new AuthView();
        authView.setListener(this);
        ((StackPane) getNamespace().get("stackPane")).getChildren().add(authView.getRoot());
    }

    @Override
    public void onAuthComplete(String ans){
        ((StackPane) getNamespace().get("stackPane")).getChildren().remove(((StackPane) getNamespace().get("stackPane")).getChildren().size()-1);
        String result[] = ans.split("&");
        String token = "&".concat(result[0]);
        int ID = Integer.parseInt(result[2].split("=")[1]);

        if (!Authorization.addAccount(ID, token)) {
            ((AccountsViewController) getController()).showError("Такой аккаунт уже существует");
            return;
        }

        AccountRecordView arv = new AccountRecordView(Authorization.curAcc);
        ((AccountsViewController) getController()).addAccount(Authorization.curAcc.ID, arv.getRoot());
        ((AccountRecordViewController) arv.getController()).setListener(this);

        AvaLoader avaLoader = new AvaLoader();
        AccountLoader loader = new AccountLoader(Authorization.curAcc, avaLoader, null);
        loader.start();
        loader.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent workerStateEvent) {
                ((AccountRecordViewController) arv.getController()).setName(loader.getValue().first_name, loader.getValue().last_name);
            }
        });
        avaLoader.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent workerStateEvent) {
                ((AccountRecordViewController) arv.getController()).setAva(avaLoader.getValue());
            }
        });
        avaLoader.addEventHandler(WorkerStateEvent.WORKER_STATE_CANCELLED, new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent workerStateEvent) {
                ((AccountsViewController) getController()).showError(loader.getException().toString());
            }
        });
    }

    @Override
    public void onAuthFailed(){
        ((StackPane) getNamespace().get("stackPane")).getChildren().remove(((StackPane) getNamespace().get("stackPane")).getChildren().size()-1);
        ((AccountsViewController) getController()).showError("Авотризация не удалась");
    }

    @Override
    public void onAccClick(int id){
        listener.onAccChoose(id);
    }

    @Override
    public void onAccDeleteClick(int id){
        Authorization.deleteAccount(id);
        ((AccountsViewController) getController()).removeAccount(id);
    }
}
