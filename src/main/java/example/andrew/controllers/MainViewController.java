package example.andrew.controllers;

import org.mt.vk.authorization.Account;
import org.mt.vk.authorization.Authorization;
import org.mt.vk.user.LightUser;
import com.jfoenix.controls.JFXSnackbar;
import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import example.andrew.Searcher;
import example.andrew.Tree;
import example.andrew.interfaces.MainViewListener;
import example.andrew.loaders.AvaLoader;
import example.andrew.views.ResultView;
import example.andrew.draw.Layout;
import example.andrew.draw.Draw;
import example.andrew.draw.DrawAll;
import example.andrew.draw.DrawConsistently;
import example.andrew.draw.DrawMatrix;
import example.andrew.draw.DrawList;

public class MainViewController {

    private MainViewListener listener;

    @FXML
    FlowPane leftPane;
    @FXML
    AnchorPane centralPane;
    @FXML
    FlowPane rightPane;

    @FXML
    ImageView imAva;
    @FXML
    Label accId;
    @FXML
    Label accFName;
    @FXML
    Label accSName;

    @FXML
    Slider slDeep;
    @FXML
    ToggleGroup tgViewType;

    private Searcher searcher;
    private ResultView resultView;

    public void setAccount(Account account){
        //загрузка toolbar
        AvaLoader loader = new AvaLoader();
        loader.start(account.avaSmall, null);
        loader.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, (workerStateEvent)->
                imAva.setImage(Layout.roundImage(loader.getValue()))
        );
        accId.setText("id".concat(Integer.toString(account.ID)));
        accFName.setText(account.first_name);
        accSName.setText(account.last_name);

        //контекстное меню для toolbar
        final ContextMenu contextMenu = new ContextMenu();
        MenuItem copy = new MenuItem("Скопировать ссылку");
        copy.setOnAction((actionEvent)-> {
            Clipboard clipboard = Clipboard.getSystemClipboard();
            ClipboardContent content = new ClipboardContent();
            content.putString("https://vk.com/id".concat(Integer.toString(Authorization.curAcc.ID)));
            clipboard.setContent(content);
        });
        MenuItem useAsFirst = new MenuItem("Использовать как 1-го пользователя");
        useAsFirst.setOnAction((actionEvent)->
                listener.loadFirstUser(Authorization.curAcc.ID)
        );
        MenuItem useAsSecond = new MenuItem("Использовать как 2-го пользователя");
        useAsSecond.setOnAction((actionEvent)->
                listener.loadSecondUser(Authorization.curAcc.ID)
        );
        SeparatorMenuItem smi = new SeparatorMenuItem();
        MenuItem exit = new MenuItem("Выйти");
        exit.setOnAction((actionEvent)->
                listener.changeAccount()
        );
        contextMenu.getItems().addAll(copy, useAsFirst, useAsSecond, smi, exit);
        imAva.setOnContextMenuRequested((contextMenuEvent)->
                contextMenu.show(imAva, contextMenuEvent.getScreenX(), contextMenuEvent.getScreenY())
        );
    }

    public void setListener(MainViewListener listener){
        this.listener = listener;
    }

    @FXML
    public void execute(){
        LightUser start = listener.getFirstUser();
        if (start == null){
            showError("Первый пользователь не задан");
            return;
        }
        LightUser finish = listener.getSecondUser();
        if (finish == null){
            showError("Второй пользователь не задан");
            return;
        }
        if (finish.ID == start.ID){
            showError("Первый и второй пользователи совпадают");
            return;
        }
        int deep = (int) slDeep.getValue();

        Stage stEnter = new Stage();
        stEnter.setTitle("Результат");
        resultView = new ResultView();
        Scene scEnter = new Scene(resultView.getRoot(), 1000, 650);
        stEnter.setScene(scEnter);
        stEnter.show();

        //если уже имеется объект searcher с такими же параметрами
        if (searcher != null && searcher.getFinishUser().ID == finish.ID &&
                searcher.getStartUser().ID == start.ID &&
                searcher.getDeep() == deep &&
                searcher.isDone()){
            resultView.setNumOfLinks(searcher.getCount());
            executeDrawing(searcher.getValue());
            return;
        }

        searcher = new Searcher(start, finish, deep);
        System.gc();
        resultView.bindNumProperty(searcher.messageProperty());
        searcher.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, (WorkerStateEvent workerStateEvent)-> {
                Tree tree = searcher.getValue();
                tree.calcLayout();
                tree.setNumOfLinks(searcher.getCount());
                System.out.println(tree);
                executeDrawing(tree);
        });
        searcher.addEventHandler(WorkerStateEvent.WORKER_STATE_FAILED, (WorkerStateEvent event) ->{
            System.out.println("Searching error");
            showError("Ошибка при поиске");
            stEnter.close();
        });
        searcher.start(System.currentTimeMillis());
    }

    private void executeDrawing(Tree tree){
        resultView.setProcessName("Отрисовка");
        Draw d;
        switch (((RadioButton) tgViewType.getSelectedToggle()).getText()){
            case "Весь граф":
                d = new DrawAll(tree);
                break;
            case "По одному элементу":
                d = new DrawConsistently(tree);
                break;
            case "Матрица смежности":
                d = new DrawMatrix(tree);
                break;
            case "Список":
                d = new DrawList(tree);
                break;
            default:
                return;
        }
        final Draw draw = d;
        draw.setListener(listener);
        draw.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, (WorkerStateEvent workerStateEvent) ->
                resultView.setContent(draw.getValue())
        );
        draw.launch();
    }

    private void showError(String message){
        JFXSnackbar error = new JFXSnackbar(centralPane);
        error.getStylesheets().add("/styles/snackbar.css");
        error.show(message, 2000);
    }
}
