package example.andrew.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.*;
import example.andrew.draw.Layout;
import example.andrew.interfaces.AccountsViewListener;

public class AccountRecordViewController {

    private AccountsViewListener listener;

    @FXML
    ImageView imAva;
    @FXML
    Label lId;
    @FXML
    Label lFName;
    @FXML
    Label lSName;

    public void setId(int id){
        lId.setText("id".concat(Integer.toString(id)));
    }

    public void setAva(Image ava){
        imAva.setImage(Layout.roundImage(ava));
    }

    public void setName(String fName, String lName){
        lFName.setText(fName);
        lSName.setText(lName);
    }

    @FXML
    public void onButDelClick(){
        listener.onAccDeleteClick(Integer.parseInt(lId.getText().substring(2)));
    }

    public void setListener(AccountsViewListener listener){
        this.listener = listener;
    }
}
