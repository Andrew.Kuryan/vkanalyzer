package example.andrew.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import example.andrew.draw.Layout;

public class NodeViewController {

    @FXML
    ImageView imAva;

    @FXML
    Label labNum;

    public void setAva(Image image){
        imAva.setImage(Layout.roundImage(image));
    }

    public void setNum(int num){
        labNum.setText(Integer.toString(num));
    }
}
