package example.andrew.controllers;

import com.jfoenix.controls.JFXSnackbar;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import example.andrew.interfaces.AccountsViewListener;

import java.util.HashMap;

public class AccountsViewController {

    private AccountsViewListener listener;

    @FXML
    AnchorPane apRoot;
    @FXML
    VBox box;

    private HashMap<Integer, Node> mapAccounts = new HashMap<>();

    public void showError(String message){
        JFXSnackbar error = new JFXSnackbar(apRoot);
        error.getStylesheets().add("/styles/snackbar.css");
        error.show(message, 3000);
    }

    public void removeAccount(int id){
        box.getChildren().remove(mapAccounts.get(id));
        mapAccounts.remove(id);
    }

    public void addAccount(int id, Node node){
        mapAccounts.put(id, node);
        box.getChildren().add(0, node);
        node.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent){
                listener.onAccClick(id);
            }
        });
    }

    @FXML
    public void onButAddClick(){
        listener.onButAddClick();
    }

    public void setListener(AccountsViewListener listener){
        this.listener = listener;
    }
}
