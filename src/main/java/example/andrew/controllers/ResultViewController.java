package example.andrew.controllers;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;

public class ResultViewController {

    @FXML
    StackPane stackPane;
    @FXML
    ScrollPane spEnter;
    @FXML
    FlowPane fpEnter;
    @FXML
    ProgressIndicator piSearch;
    @FXML
    Label labState;

    @FXML
    Label numOfLinks;

    public void setContent(Node content){
        spEnter.setContent(content);
    }

    public void bindNumProperty(ReadOnlyStringProperty property){
        numOfLinks.textProperty().bind(property);
    }

    public void setNumOfLinks(int num){
        numOfLinks.setText(Integer.toString(num));
    }

    public void setProcessName(String name){
        labState.setText(name);
    }
}
