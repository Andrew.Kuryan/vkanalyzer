package example.andrew.controllers;

import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import example.andrew.interfaces.AuthViewListener;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AuthViewController {

    private AuthViewListener listener;

    @FXML
    WebView webView;
    private WebEngine engine;

    public void loadAuth(){
        engine = webView.getEngine();
        engine.load("https://oauth.vk.com/authorize?client_id=3116505&scope=1073741823&redirect_uri=https://api.vk.com/blank.html&display=page&response_type=token&revoke=1");
        Pattern token = Pattern.compile("access_token=");
        Pattern error = Pattern.compile("error=");
        engine.getLoadWorker().stateProperty().addListener((obs, oldState, newState) -> {
            String url = engine.getLocation();
                if (newState == Worker.State.FAILED)
                //listener.onAuthFailed();
            System.out.println(url);
            Matcher matcher = token.matcher(url);
            if (matcher.find()){
                String ans;
                int start = matcher.start();
                ans = url.substring(start);
                listener.onAuthComplete(ans);
            }
            matcher = error.matcher(url);
            if (matcher.find()){
                listener.onAuthFailed();
            }
        });
    }

    @FXML
    public void onButReset(){
        listener.onButReset();
    }

    @FXML
    public void onButReload(){
        engine.reload();
    }

    public void setListener(AuthViewListener listener){
        this.listener = listener;
    }
}
