package example.andrew.controllers;

import org.mt.vk.user.User;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXSnackbar;
import com.jfoenix.transitions.hamburger.HamburgerBasicCloseTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import example.andrew.loaders.AvaLoader;
import example.andrew.loaders.UserLoader;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

public class UserViewController implements Initializable {

    private static double listWidth;
    private static SimpleDateFormat lastSeenFormat = new SimpleDateFormat("HH:mm");

    @FXML
    AnchorPane apRoot;

    @FXML
    JFXDrawer drawer;
    @FXML
    JFXHamburger hOpen;
    @FXML
    FlowPane boxFind;
    @FXML
    FlowPane fpAddition;
    @FXML
    TextField tfId;

    @FXML
    ImageView imAva;
    @FXML
    ProgressIndicator piAva;

    @FXML
    VBox boxName;
    @FXML
    Label lFName;
    @FXML
    Label lSName;
    @FXML
    Label lNumFriends;
    @FXML
    Label lNumFollows;
    @FXML
    Label lNumGroups;

    @FXML
    ListView<AnchorPane> listMainInfo;

    private HamburgerBasicCloseTransition transition;

    public User user;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle){
        boxFind.setVisible(true);
        drawer.setSidePane(boxFind);
        transition = new HamburgerBasicCloseTransition(hOpen);
        transition.setRate(-1);
    }

    public void onButEnterClick(){
        String input = tfId.getText();
        loadUser(input);
    }

    public void loadUser(String screen_name){
        if (screen_name == null || screen_name.trim().isEmpty()){
            JFXSnackbar error = new JFXSnackbar(apRoot);
            error.getStylesheets().add("main/resources/styles/snackbar.css");
            error.show("Пустое поле ID", 2000);
            return;
        }
        AvaLoader loader = new AvaLoader();
        UserLoader userLoader = new UserLoader(screen_name, loader, piAva);
        userLoader.start();
        loader.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, (WorkerStateEvent workerStateEvent)->
                imAva.setImage(loader.getValue())
        );
        userLoader.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, (WorkerStateEvent workerStateEvent)-> {
                user = userLoader.getValue();
                tfId.setText(Integer.toString(user.ID));
                lFName.setText(user.first_name);
                lSName.setText(user.last_name);
                if (user.numFriends >= 0)
                    lNumFriends.setText(Integer.toString(user.numFriends));
                else
                    lNumFriends.setText("-");
                if (user.numFollows >= 0)
                    lNumFollows.setText(Integer.toString(user.numFollows));
                else
                    lNumFollows.setText("-");
                if (user.numGroups >= 0)
                    lNumGroups.setText(Integer.toString(user.numGroups));
                else
                    lNumGroups.setText("-");

                ObservableList<AnchorPane> list = FXCollections.observableArrayList();
                listWidth = listMainInfo.getWidth();
                if (!user.status.isEmpty()){
                    list.add(createRecord("Статус:", user.status));
                }
                if (user.online == 1)
                    list.add(createRecord("В сети:", "Онлайн"));
                else
                    list.add(createRecord("В сети:", lastSeenFormat.format(new Date(user.last_seen * 1000))));
                if (!user.bdate.isEmpty())
                    list.add(createRecord("Дата рождения:", user.bdate));
                if (!user.site.isEmpty())
                    list.add(createRecord("Сайт:", user.site));
                if (!user.country.isEmpty())
                    list.add(createRecord("Страна:", user.country));
                if (!user.city.isEmpty())
                    list.add(createRecord("Город:", user.city));
                if (!user.occupType.isEmpty())
                    list.add(createRecord(user.getOccupType().concat(":"), user.occupName));
                listMainInfo.setItems(list);

                if (drawer.isOpened()){
                    transition.setRate(transition.getRate()*(-1));
                    transition.play();
                    drawer.close();
                }
        });
        userLoader.addEventHandler(WorkerStateEvent.WORKER_STATE_FAILED, (WorkerStateEvent workerStateEvent)-> {
                JFXSnackbar error = new JFXSnackbar(apRoot);
                error.getStylesheets().add("main/resources/styles/snackbar.css");
                error.show(userLoader.getException().toString(), 2000);
        });
    }

    @FXML
    public void onButOpen(){
        switchDrawer();
    }

    private void switchDrawer(){
        transition.setRate(transition.getRate()*(-1));
        transition.play();
        if (drawer.isClosed())
            drawer.open();
        else
            drawer.close();
    }

    private AnchorPane createRecord(String name, String val){
        final double gap = 2.0;
        AnchorPane ap = new AnchorPane();
        Label l1 = new Label(name);
        l1.setWrapText(true);
        l1.setMaxWidth(listWidth/2-gap-gap);
        l1.setTextAlignment(TextAlignment.LEFT);
        Label l2 = new Label(val);
        l2.setWrapText(true);
        l2.setMaxWidth(listWidth/2-gap-gap);
        l2.setTextAlignment(TextAlignment.RIGHT);
        ap.getChildren().addAll(l1, l2);
        AnchorPane.setLeftAnchor(l1, gap);
        AnchorPane.setRightAnchor(l2, gap);
        return ap;
    }
}
