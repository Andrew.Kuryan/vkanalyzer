package example.andrew.interfaces;

public interface AccountsViewListener {

    void onButAddClick();
    void onAuthComplete(String ans);
    void onAuthFailed();
    void onAccClick(int id);
    void onAccDeleteClick(int id);
}
