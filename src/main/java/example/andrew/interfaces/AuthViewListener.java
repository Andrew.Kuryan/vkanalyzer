package example.andrew.interfaces;

public interface AuthViewListener {

    void onButReset();
    void onAuthComplete(String ans);
    void onAuthFailed();
}
