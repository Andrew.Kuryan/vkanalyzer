package example.andrew.interfaces;

import org.mt.vk.user.User;

public interface MainViewListener {

    void loadFirstUser(int id);
    void loadSecondUser(int id);
    void changeAccount();
    User getFirstUser();
    User getSecondUser();
}
