package example.andrew.draw;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;

public class Layout {

    public static final double nodeWidth = 50;
    public static final double nodeHeight = 59;
    public static final double photoVerOffset = nodeWidth / 2;
    public static final double photoHorOffset = nodeHeight / 2;
    public static final double linkOffset = nodeHeight - photoVerOffset + 5;
    public static final double xGap = 20;
    public static final double yGap = 40;

    public static final double matrVMargin = 5;
    public static final double matrHMargin = 8;

    public static Image roundImage(Image old){
        double width = old.getWidth();
        double height = old.getHeight();
        WritableImage wImage = new WritableImage((int) width, (int) height);
        PixelWriter writer = wImage.getPixelWriter();
        PixelReader pixelReader = old.getPixelReader();
        for(int y = 0; y < height; y++) {
            for(int x = 0; x < width; x++) {
                double r = Math.sqrt(Math.pow((x-width/2), 2) + Math.pow((y-height/2),2));
                javafx.scene.paint.Color color = pixelReader.getColor(x, y);
                if (r > height/2)
                    writer.setColor(x, y, Color.TRANSPARENT);
                else
                    writer.setColor(x, y, color);
            }
        }
        return wImage;
    }

    public static FlowPane getNullMessage(){
        FlowPane flowPane = new FlowPane();
        flowPane.getStylesheets().add("/styles/fonts.css");
        flowPane.setAlignment(Pos.CENTER);
        Label mes = new Label("Для указанной глубины поиска связей не найдено");
        mes.getStyleClass().add("null-mes-font");
        flowPane.getChildren().add(mes);
        return flowPane;
    }
}
