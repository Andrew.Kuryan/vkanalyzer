package example.andrew.draw;

import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import example.andrew.Tree;
import example.andrew.views.NodeView;

public class DrawMatrix extends Draw{

    public DrawMatrix(Tree tree){
        super(tree);
    }

    protected FlowPane call(){
        Platform.runLater(() -> {
            if (tree.e.size() == 0){
                fpContent.getChildren().add(Layout.getNullMessage());
                return;
            }
            GridPane grid = new GridPane();
            grid.getStylesheets().add(getClass().getResource("/styles/shapes.css").toString());
            grid.setGridLinesVisible(true);
            //отрисовка юнитов
            for (int i=0; i<tree.u.size(); i++){
                NodeView nodeView = new NodeView(tree.u.get(i));
                GridPane.setValignment(nodeView.getNode(), VPos.CENTER);
                GridPane.setHalignment(nodeView.getNode(), HPos.CENTER);
                GridPane.setMargin(nodeView.getNode(),
                        new Insets(Layout.matrVMargin, Layout.matrHMargin, Layout.matrVMargin, Layout.matrHMargin));
                grid.add(nodeView.getNode(), 0, i+1);

                NodeView nodeView1 = new NodeView(tree.u.get(i));
                GridPane.setValignment(nodeView1.getNode(), VPos.CENTER);
                GridPane.setHalignment(nodeView1.getNode(), HPos.CENTER);
                GridPane.setMargin(nodeView1.getNode(),
                        new Insets(Layout.matrVMargin, Layout.matrHMargin, Layout.matrVMargin, Layout.matrHMargin));
                grid.add(nodeView1.getNode(), i+1, 0);
            }
            //отрисовка связей
            for (Tree.Edge edge : tree.e){
                int xPos = 0, yPos = 0;
                for (int i=0; i<tree.u.size(); i++)
                    if (edge.vert1.ID == tree.u.get(i).ID)
                        xPos = i+1;
                for (int i=0; i<tree.u.size(); i++)
                    if (edge.vert2.ID == tree.u.get(i).ID)
                        yPos = i+1;
                Circle cir1 = new Circle(Layout.nodeWidth /2.5);
                GridPane.setValignment(cir1, VPos.CENTER);
                GridPane.setHalignment(cir1, HPos.CENTER);
                grid.add(cir1, xPos, yPos);
                cir1.getStyleClass().add("matr-point");

                Circle cir2 = new Circle(Layout.nodeWidth /2.5);
                GridPane.setValignment(cir2, VPos.CENTER);
                GridPane.setHalignment(cir2, HPos.CENTER);
                grid.add(cir2, yPos, xPos);
                cir2.getStyleClass().add("matr-point");
            }
            for (int i=0; i<=tree.u.size(); i++){
                Rectangle rect = new Rectangle(Layout.nodeWidth + 2 * Layout.matrHMargin,
                                                Layout.nodeHeight + 2 * Layout.matrVMargin);
                grid.add(rect, i, i);
                rect.getStyleClass().add("matr-null");
            }
            fpContent.getChildren().add(grid);
        });
        return fpContent;
    }
}
