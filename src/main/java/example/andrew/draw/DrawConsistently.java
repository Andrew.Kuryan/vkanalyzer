package example.andrew.draw;

import javafx.application.Platform;
import javafx.scene.layout.FlowPane;
import javafx.scene.shape.Line;
import example.andrew.views.NodeView;
import example.andrew.Tree;

import java.util.ArrayList;

public class DrawConsistently extends Draw{

    private ArrayList<Tree.Unit> chain;

    public DrawConsistently(Tree tree){
        super(tree);
        maxWidth = tree.getMaxCount() * Layout.nodeWidth + (tree.getMaxCount() - 1) * Layout.xGap;
        maxHeight = tree.getNumOfLevels() * Layout.nodeHeight + (tree.getNumOfLevels() - 1) * Layout.yGap;
    }

    protected FlowPane call(){
        Platform.runLater(() -> {
            if (tree.e.size() == 0){
                fpContent.getChildren().add(Layout.getNullMessage());
                return;
            }
            paneUsers.setPrefSize(maxWidth, maxHeight);
            paneLinks.setPrefSize(maxWidth, maxHeight);
            Tree.Unit temp = tree.u.get(0);
            chain = new ArrayList<>();
            chain.add(temp);

            NodeView nodeView = new NodeView(temp);
            nodeView.getNode().setOnMouseClicked((mouseEvent)->
                    action(temp)
            );
            nodeView.setListener(listener);

            nodeView.getNode().setLayoutX(getX(1, 0) - Layout.photoVerOffset);
            nodeView.getNode().setLayoutY(getY(0) - Layout.photoVerOffset);

            paneUsers.getChildren().addAll(nodeView.getNode());
            paneCommon.getChildren().add(paneLinks);
            paneCommon.getChildren().add(paneUsers);
            fpContent.getChildren().add(paneCommon);
        });
        return fpContent;
    }

    private double getX(int amnt, int pos){
        return (maxWidth - Layout.nodeWidth * amnt - Layout.xGap * (amnt-1)) / 2
                + (Layout.nodeWidth + Layout.xGap) * pos
                + Layout.photoVerOffset;
    }

    private double getY(int lev){
        return lev * (Layout.nodeHeight + Layout.yGap) + Layout.photoVerOffset;
    }

    private void action(Tree.Unit u){
            paneUsers.getChildren().clear();
            paneLinks.getChildren().clear();
            double xPos, yPos, x0 = getX(1, 0);
            boolean flag = false;
            int count = 0;
            Tree.Unit temp;
            if (u.numOfChildrens > 0) {
                Line offset = new Line(x0, getY(u.level), x0, getY(u.level) + Layout.linkOffset);
                paneLinks.getChildren().add(offset);
            }
            if (u.numOfChildrens > 1){
                Line link = new Line(getX(u.numOfChildrens, 0),
                                     getY(u.level) + Layout.linkOffset,
                                     getX(u.numOfChildrens, u.numOfChildrens - 1),
                                     getY(u.level) + Layout.linkOffset);
                paneLinks.getChildren().addAll(link);
            }
            //отрисовка всех дочерних элементов выбранного
            for (int i = 0; i < tree.e.size(); i++) {
                if (tree.e.get(i).vert1.ID == u.ID) {
                    temp = tree.e.get(i).vert2;
                    xPos = getX(u.numOfChildrens, count);
                    count++;
                    yPos = getY(u.level + 1);
                    l = new Line(xPos, getY(u.level)+Layout.linkOffset, xPos, yPos);

                    final Tree.Unit t1 = temp;
                    NodeView nodeView = new NodeView(temp);
                    nodeView.getNode().setOnMouseClicked((mouseEvent)->
                            action(t1)
                    );
                    nodeView.setListener(listener);
                    nodeView.getNode().setLayoutX(xPos - Layout.photoVerOffset);
                    nodeView.getNode().setLayoutY(yPos - Layout.photoVerOffset);

                    paneLinks.getChildren().add(l);
                    paneUsers.getChildren().addAll(nodeView.getNode());
                }
            }
            //отрисовка элементов в цепочке выше выбранного
            xPos = x0;
            for (int i = 0; i < chain.size(); i++) {
                temp = chain.get(i);
                yPos = getY(i);
                if (temp.ID == u.ID)
                    flag = true;
                l = new Line(xPos, yPos, xPos, getY(i + 1));
                if (!flag)
                    paneLinks.getChildren().add(l);

                final Tree.Unit t1 = temp;
                NodeView nodeView = new NodeView(temp);
                nodeView.getNode().setOnMouseClicked((mouseEvent)->
                        action(t1)
                );
                nodeView.setListener(listener);
                nodeView.getNode().setLayoutX(xPos - Layout.photoVerOffset);
                nodeView.getNode().setLayoutY(yPos - Layout.photoVerOffset);
                paneUsers.getChildren().add(nodeView.getNode());

                if (flag) {
                    for (int f = i + 1; f < chain.size(); )
                        chain.remove(f);
                    break;
                }
            }
            //если выбран элемент, которого нет в цепочке
            if (!flag) {
                chain.add(u);
                temp = chain.get(chain.size() - 1);
                yPos = getY(chain.size() - 1);

                final Tree.Unit t1 = temp;
                NodeView nodeView = new NodeView(temp);
                nodeView.getNode().setOnMouseClicked((mouseEvent)->
                        action(t1)
                );
                nodeView.setListener(listener);
                nodeView.getNode().setLayoutX(xPos - Layout.photoVerOffset);
                nodeView.getNode().setLayoutY(yPos - Layout.photoVerOffset);
                paneUsers.getChildren().add(nodeView.getNode());
            }
    }
}
