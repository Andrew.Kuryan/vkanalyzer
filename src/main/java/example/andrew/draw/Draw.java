package example.andrew.draw;

import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.layout.*;
import javafx.scene.shape.Line;
import example.andrew.Tree;
import example.andrew.interfaces.MainViewListener;

public abstract class Draw extends Task<FlowPane> {

    MainViewListener listener;
    Tree tree;
    double maxWidth, maxHeight;
    Tree.Unit temp1, temp2;
    FlowPane fpContent;
    Pane paneCommon;
    Pane paneUsers;
    Pane paneLinks;
    Line l;

    public Draw(Tree tree){
        this.tree = tree;
        fpContent = new FlowPane(0, 0);
        fpContent.setAlignment(Pos.CENTER);
        paneCommon = new Pane();
        paneUsers = new Pane();
        paneLinks = new Pane();
    }

    public void launch(){
        new Thread(this).start();
    }

    public void setListener(MainViewListener listener){
        this.listener = listener;
    }
}
