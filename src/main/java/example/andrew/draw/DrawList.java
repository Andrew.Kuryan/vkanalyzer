package example.andrew.draw;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import example.andrew.Tree;
import example.andrew.views.NodeView;

import java.util.ArrayList;

public class DrawList extends Draw{

    public DrawList(Tree tree){
        super(tree);
        maxWidth = 2 * Layout.xGap + tree.getNumOfLevels() * Layout.nodeWidth + (tree.getNumOfLevels()-1) * Layout.xGap;
        maxHeight = tree.getNumOfLinks() * Layout.nodeHeight + (tree.getNumOfLinks() - 1) * Layout.xGap;
    }

    protected FlowPane call(){
        Platform.runLater(() -> {
            if (tree.e.size() == 0){
                fpContent.getChildren().add(Layout.getNullMessage());
                return;
            }
            paneUsers.setPrefSize(maxWidth, maxHeight);
            paneLinks.setPrefSize(maxWidth, maxHeight);
            Label labC;
            //отрисовка последнего юнита
            for (int i = 0; i< tree.getNumOfLinks(); i++){
                labC = new Label(Integer.toString(i+1)+". ");
                labC.setLayoutY(getY(i) - Layout.xGap / 2);
                labC.setFont(Font.font("Noto Sans", FontWeight.BOLD, 18));
                paneUsers.getChildren().add(labC);

                NodeView nodeFinish = new NodeView(tree.getStartUser());
                nodeFinish.getNode().setLayoutX(getX(0) - Layout.photoHorOffset);
                nodeFinish.getNode().setLayoutY(getY(i) - Layout.photoVerOffset);
                paneUsers.getChildren().add(nodeFinish.getNode());
            }
            //отрисовка юнитов
            for (int i=0; i<tree.listOfLinks.size(); i++){
                ArrayList<Tree.Edge> link = tree.listOfLinks.get(i);
                for (Tree.Edge e : link){
                    Tree.Unit unit = e.vert2;
                    double xPos = getX(unit.level);
                    double yPos = getY(i);

                    NodeView nodeView = new NodeView(unit);
                    nodeView.getNode().setLayoutX(xPos - Layout.photoHorOffset);
                    nodeView.getNode().setLayoutY(yPos - Layout.photoVerOffset);
                    paneUsers.getChildren().add(nodeView.getNode());
                }
            }
            //отрисовка связей
            int vGap = 0;
            double xStart = getX(0);
            double xEnd = getX(tree.getNumOfLevels()-1);
            for (int i=0; i<tree.getNumOfLinks(); i++){
                l = new Line(xStart, getY(i), xEnd, getY(i));
                vGap++;
                paneLinks.getChildren().add(l);
            }
            paneCommon.getChildren().add(paneLinks);
            paneCommon.getChildren().add(paneUsers);
            fpContent.getChildren().add(paneCommon);
        });
        return fpContent;
    }

    private double getX(int lev){
        double value = 2 * Layout.xGap + lev * Layout.nodeWidth + lev * Layout.xGap + Layout.photoHorOffset;
        System.out.println("getX "+lev+" : "+value);
        return value;
    }

    private double getY(int pos){
        double value = pos * Layout.nodeHeight + pos * Layout.xGap + Layout.photoVerOffset;
        System.out.println("getY "+pos+" : "+value);
        return value;
    }
}
