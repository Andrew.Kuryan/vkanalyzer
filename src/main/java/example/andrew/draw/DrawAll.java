package example.andrew.draw;

import javafx.application.Platform;
import javafx.scene.layout.FlowPane;
import javafx.scene.shape.Line;
import example.andrew.views.NodeView;
import example.andrew.Tree;

public class DrawAll extends Draw{

    public DrawAll(Tree tree){
        super(tree);
        maxWidth = tree.getMaxLevelCount() * Layout.nodeWidth + (tree.getMaxLevelCount() - 1) * Layout.xGap;
        maxHeight = tree.getNumOfLevels() * Layout.nodeHeight + (tree.getNumOfLevels() - 1) * Layout.yGap;
        System.out.println("Max Width: "+maxWidth);
        System.out.println("Max Height: "+maxHeight);
        for (Tree.Unit unit : tree.u)
            System.out.println(unit.ID+ " " + unit.level);
    }

    protected FlowPane call(){
        Platform.runLater(()-> {
            System.out.println("E size: "+tree.e.size());
            if (tree.e.size() == 0){
                System.out.println("No edges");
                fpContent.getChildren().add(Layout.getNullMessage());
                return;
            }
            paneUsers.setPrefSize(maxWidth, maxHeight);
            paneLinks.setPrefSize(maxWidth, maxHeight);
            double xPos, yPos;
            for (int i = 0; i < tree.e.size(); i++) {
                temp1 = tree.e.get(i).vert1;
                temp2 = tree.e.get(i).vert2;

                l = new Line(getX(temp1), getY(temp1.level), getX(temp2), getY(temp2.level));

                paneLinks.getChildren().add(l);
            }

            for (int i = 0; i < tree.u.size(); i++) {
                temp1 = tree.u.get(i);
                xPos = getX(temp1); yPos = getY(temp1.level);
                if (temp1.ID == tree.finishID && tree.getNumOfUnits(temp1.level)>1){
                    xPos = getX(1, 0);
                    yPos = getY(temp1.level+1);
                }
                NodeView nodeView = new NodeView(temp1);
                nodeView.setListener(listener);
                nodeView.getNode().setLayoutX(xPos - Layout.photoVerOffset);
                nodeView.getNode().setLayoutY(yPos - Layout.photoVerOffset);
                paneUsers.getChildren().add(nodeView.getNode());
            }

            paneCommon.getChildren().add(paneLinks);
            paneCommon.getChildren().add(paneUsers);
            fpContent.getChildren().add(paneCommon);
        });
        return fpContent;
    }

    private double getX(int amnt, int pos) {
        double val = (maxWidth - Layout.nodeWidth * amnt - Layout.xGap * (amnt-1)) / 2
                + (Layout.nodeWidth + Layout.xGap) * pos + Layout.photoVerOffset;
        System.out.println("amnt: "+amnt + "; pos: "+pos + "; return: "+val);
        return val;
    }

    private double getX(Tree.Unit u){
        int num = tree.getNumOfUnits(u.level);
        int posInLevel = 0;
        for (int i = 0; i < tree.u.size(); i++) {
            if (tree.u.get(i).level == u.level && tree.u.get(i).ID != u.ID)
                posInLevel++;
            else if (tree.u.get(i).ID == u.ID)
                break;
        }
        return getX(num, posInLevel);
    }

    private double getY(int lev) {
        return lev * (Layout.nodeHeight + Layout.yGap) + Layout.photoVerOffset;
    }
}
