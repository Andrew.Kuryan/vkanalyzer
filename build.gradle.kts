plugins {
    java
    application
}

val fxModules = arrayListOf("base", "graphics", "controls", "fxml", "web", "media")

tasks.withType<JavaCompile>().all {
    doFirst {
        options.compilerArgs = mutableListOf(
                "--module-path", classpath.asPath,
                "--add-modules", fxModules.joinToString(separator = ",") { "javafx.$it" })
    }
}

application {
    tasks.withType<JavaExec>().all {
        doFirst {
            jvmArgs = mutableListOf(
                    "--module-path", classpath.asPath,
                    "--add-modules", fxModules.joinToString(separator = ",") { "javafx.$it" })
        }
    }
    mainClassName = "example.andrew.Main"
}

val platform = with(org.gradle.internal.os.OperatingSystem.current()) {
    when {
        isWindows -> "win"
        isLinux -> "linux"
        isMacOsX -> "mac"
        else -> "unknown"
    }
}

repositories {
    mavenCentral()
    mavenLocal()
    jcenter()
}

val jfxVersion = "9.0.6"
val vkApiVersion = "2.1.0"

dependencies {
    for (module in fxModules) {
        implementation("org.openjfx:javafx-$module:11:$platform")
    }
    implementation("com.jfoenix:jfoenix:$jfxVersion")
    implementation("org.mt:vk:$vkApiVersion")
}